#!/usr/bin/env python
#Author : Binaya Joshi

#This takes file "list" as input with list of all DNS records and their name servers in and output file

import dns.resolver
fout=open("output.txt","w")
with open ("list") as myfile:
 for fp in myfile:
        name=fp.rstrip()
        try: 
	   answers = dns.resolver.query(name, 'ns')
           fout.write ("Name servers for "+name+" domain\n")
           fout.write (" \n")
	   for rdata in answers:
                result=str(rdata)+"\n"
      		fout.write (result)
           fout.write ("-----------------------\n")
        except:
           print ("NS records for {} not found ".format (name))
           fout.write ("Name server for "+name+" not found\n")
           fout.write ("-----------------------\n")
fout.close()
