## Details
This program queries DNS to find zones with missing NS records. If NS is present, it is also listed.

Note: Can be easily modified to list only zones with missing NS records.

## Requirements
A file named "list" containing all zones is needed. A sample file has been provided in the repo

## Usage
```
python nsfind.py
```
## Output
```
NS records for 321homefurniture.com not found
NS records for aampmuseum.com not found
NS records for alldayfurnitures.com not found
```

## Author
Binaya Joshi
